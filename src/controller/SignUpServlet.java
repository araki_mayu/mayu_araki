package controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/sign_up" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();
		request.setAttribute("branchList", branchList);
		request.setAttribute("positionList", positionList);
		request.getRequestDispatcher("sign_up.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		List<Branch> branchList = new BranchService().select();
		List<Position> positionList = new PositionService().select();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

			new UserService().register(user);
			session.setAttribute("users", user);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
			request.setAttribute("user", user);
			request.setAttribute("branchList", branchList);
			request.setAttribute("positionList", positionList);
			request.getRequestDispatcher("sign_up.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		boolean isUniqueLoginId = new UserService().isUniqueLoginId(loginId);

		if (StringUtils.isEmpty(loginId) == true ) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("^[a-zA-Z0-9]{6,20}$")) {
			messages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
		} else if (!isUniqueLoginId) {
			messages.add("既に登録されたログインIDです");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if(!password.matches("^[a-zA-Z0-9-_@+*;:#$%&]{6,20}$")) {
			messages.add("パスワードは6文字以上20文字以下の記号を含む全ての半角文字で入力してください");
		}
		if (StringUtils.isEmpty(passwordConfirm) == true) {
			messages.add("確認用パスワードを入力してください");
		}
		if(!(password.matches(passwordConfirm))) {
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages .add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
