<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Sign_Up</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="sign_up" method="post">
				<a>新規登録する</a> <br />

				<label for="loginId">ログインID</label> <br />
				<input name="loginId" value="${user.loginId}" id="loginId" /> <br />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password" /> <br />

				<label for="passwordConfirm">パスワード確認</label> <br />
				<input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />

				<label for="name">名前</label> <br />
			    <input name="name" type="text" value="${user.name}" id="name"> <br />

			    <label for="branchId">支店名</label> <br />
				<select name="branchId">
					<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.branchId == user.branchId}">
							<option value="${branch.branchId}"selected>
								<c:out value="${branch.branchId} . ${branch.branchName}" />
							</option>
						</c:if>
						<c:if test="${branch.branchId != user.branchId }">
							<option value="${branch.branchId}">
								<c:out value="${branch.branchId} . ${branch.branchName}" />
							</option>
						</c:if>
					</c:forEach>
				</select><br />

				<label for="positionId">部署・役職</label> <br />
				<select name="positionId">
					<c:forEach items="${positionList}" var="position">
						<c:if test="${position.positionId == user.positionId}">
							<option value="${position.positionId}"selected>
								<c:out value="${position.positionId} . ${position.positionName}" />
							</option>
						</c:if>
						<c:if test="${position.positionId != user.positionId }">
							<option value="${position.positionId}">
								<c:out value="${position.positionId} . ${position.positionName}" />
							</option>
						</c:if>
					</c:forEach>
				</select> <br />

				<input type="submit" value="新規登録完了"> <br /> <a href="./">戻る</a>
			</form>
		</div>
	</body>
</html>