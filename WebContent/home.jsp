<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Home</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="sign_up">新規登録</a>
			</div>
			<table border="1">
				<tr>
					<th>ログインID</th>
					<th>名前</th>
					<th>支店名</th>
					<th>役職</th>
					<th>ユーザー</th>
					<th>編集</th>
				</tr>
				<c:forEach items="${users}" var="user">
					<tr>
						<td class="loginId"><c:out value="${user.loginId}" /></td>
						<td class="name"><c:out value="${user.name}" /></td>
						<td class="branchName"><c:out value="${user.branchName}" /></td>
						<td class="posiitonName"><c:out value="${user.positionName}" /></td>
						<td class="status">
						<form method="post">
							<c:choose>
	    						<c:when test="${user.status==0}">
	    							<input name="id" value="${user.id}" type="hidden"/>
	    							<input name="status" value="1" type="hidden"/>
									<input type="submit" onClick="return confirm('復活しますか？');" value="復活する">
	    						</c:when>
	    						<c:when test="${user.status==1}">
	    							<input name="id" value="${user.id}" type="hidden"/>
	    							<input name="status" value="0" type="hidden"/>
	    							<input type="submit" onClick="return confirm('停止しますか？');" value="停止する">
	    					     </c:when>
	    					</c:choose>
	    				</form>
	    				</td>
						<td><a href="<c:url value="/edit">
		                	<c:param name="id" value="${user.id}" />
		                	</c:url>">ユーザー編集</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</body>
</html>